package com.kypseli.beerscanner

import com.kypseli.beerscanner.beerlist.data.Beer
import com.kypseli.beerscanner.beerlist.data.BeersCollection
import com.kypseli.beerscanner.beerlist.data.BeersRepository
import com.kypseli.beerscanner.beerlist.data.IBeerListApi
import com.kypseli.beerscanner.common.data.IModelMapper
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import okhttp3.Request
import org.junit.Before
import org.junit.Test
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import org.junit.Assert.assertEquals

class BeersRepositoryUnitTest {

    private lateinit var sut: BeersRepository

    private val beerListApiStub: IBeerListApi = mock()
    private val beersCollectionMapperStub: IModelMapper<BeersCollection,
            com.kypseli.beerscanner.beerlist.domain.BeersCollection> = mock()

    @Before
    fun setUp() {
        sut = BeersRepository(beerListApiStub, beersCollectionMapperStub)
    }

    @Test
    fun getBeersByName_calledWithBeerName_beersCollectionReturned() {
        setupBeerListApiStub()

        whenever(
            beersCollectionMapperStub.fromDataModelToDomainModel(
                TestConstants.TEST_BEERS_COLLECTION_DATA_MODEL
            )
        ).thenReturn(
            TestConstants.TEST_BEERS_COLLECTION_DOMAIN_MODEL
        )

        val actual = sut.getBeersByName(TestConstants.FAKE_BEER_NAME)
        val expected = TestConstants.TEST_BEERS_COLLECTION_DOMAIN_MODEL

        assertEquals(expected, actual)
    }

    private fun setupBeerListApiStub() {
        whenever(
            beerListApiStub.getBeersByName(
                TestConstants.FAKE_BEER_NAME
            )
        ).thenReturn(
            object : Call<List<Beer>> {
                override fun enqueue(callback: Callback<List<Beer>>) {
                }

                override fun isExecuted(): Boolean {
                    return false
                }

                override fun clone(): Call<List<Beer>> {
                    return this
                }

                override fun isCanceled(): Boolean {
                    return false
                }

                override fun cancel() {
                }

                override fun execute(): Response<List<Beer>> {
                    return Response.success(TestConstants.TEST_BEERS_COLLECTION_DATA_MODEL.beers)
                }

                override fun request(): Request {
                    return Request.Builder().build()
                }

            }
        )
    }
}
