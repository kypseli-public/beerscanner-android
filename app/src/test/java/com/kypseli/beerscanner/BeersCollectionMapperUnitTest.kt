package com.kypseli.beerscanner

import com.kypseli.beerscanner.beerlist.data.BeersCollectionMapper
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class BeersCollectionMapperUnitTest {

    private lateinit var sut: BeersCollectionMapper

    @Before
    fun setUp() {
        sut = BeersCollectionMapper()
    }

    @Test
    fun fromDataModelToDomainModel_calledWithDataModel_returnDomainModel() {
        val expected = TestConstants.TEST_BEERS_COLLECTION_DOMAIN_MODEL
        val actual = sut.fromDataModelToDomainModel(
            TestConstants.TEST_BEERS_COLLECTION_DATA_MODEL
        )
        assertEquals(expected, actual)
    }
}
