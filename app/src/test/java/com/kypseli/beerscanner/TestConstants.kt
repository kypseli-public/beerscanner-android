package com.kypseli.beerscanner

import com.kypseli.beerscanner.beerlist.domain.Beer
import com.kypseli.beerscanner.beerlist.domain.BeersCollection

class TestConstants {

    companion object {

        const val FAKE_BEER_NAME = "fakeName"

        private const val FAKE_BEER_ID = 1
        private const val FAKE_TAGLINE = "fakeTagline"
        private const val FAKE_DESCRIPTION = "fakeDescription"
        private const val FAKE_ABV = 4.7F
        private const val FAKE_IMAGE_URL = "fakeImageUrl"

        private val testBeerDataModel = com.kypseli.beerscanner.beerlist.data.Beer(
            id = FAKE_BEER_ID,
            name = FAKE_BEER_NAME,
            tagline = FAKE_TAGLINE,
            description = FAKE_DESCRIPTION,
            abv = FAKE_ABV,
            imageUrl = FAKE_IMAGE_URL
        )

        val TEST_BEERS_COLLECTION_DATA_MODEL =
            com.kypseli.beerscanner.beerlist.data.BeersCollection(
                arrayListOf(
                    testBeerDataModel,
                    testBeerDataModel.copy(id = testBeerDataModel.id + 1)
                )
            )

        private val testBeerDomainModel = Beer(
            id = FAKE_BEER_ID,
            name = FAKE_BEER_NAME,
            tagline = FAKE_TAGLINE,
            description = FAKE_DESCRIPTION,
            abv = FAKE_ABV,
            imageUrl = FAKE_IMAGE_URL
        )

        val TEST_BEERS_COLLECTION_DOMAIN_MODEL = BeersCollection(
            arrayListOf(
                testBeerDomainModel,
                testBeerDomainModel.copy(id = testBeerDomainModel.id + 1)
            )
        )
    }
}
