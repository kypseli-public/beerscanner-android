package com.kypseli.beerscanner

import com.kypseli.beerscanner.beerlist.domain.BeersCollection
import com.kypseli.beerscanner.beerlist.domain.GetBeersByNameUseCase
import com.kypseli.beerscanner.beerlist.domain.IBeersRepository
import com.kypseli.beerscanner.common.domain.ConnectionRequiredUseCaseRunner
import com.kypseli.beerscanner.common.domain.Error
import com.kypseli.beerscanner.common.domain.UseCaseResult
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class GetBeersByNameUseCaseUnitTest {

    private lateinit var sut: GetBeersByNameUseCase

    private val connectionRequiredUseCaseRunnerStub
            : ConnectionRequiredUseCaseRunner<BeersCollection, Error> = mock()

    @Before
    fun setUp() {
        val beerRepositoryDummy: IBeersRepository = mock()
        sut = GetBeersByNameUseCase(
            connectionRequiredUseCaseRunnerStub,
            beerRepositoryDummy
        )
    }

    @Test
    fun getBeersByName_calledWhenUseCaseResultIsSuccess_returnSuccessUseCaseResult() =
        assertGetBeersByNameUseCaseResult(
            UseCaseResult.Success(
                TestConstants.TEST_BEERS_COLLECTION_DOMAIN_MODEL
            )
        )

    @Test
    fun getBeersByName_calledWhenUseCaseResultIsFailure_returnFailureUseCaseResult() =
        assertGetBeersByNameUseCaseResult(
            UseCaseResult.Failure(
                Error.UnknownError("anError")
            )
        )

    private fun assertGetBeersByNameUseCaseResult(
        connectionRequiredUseCaseRunnerResult: UseCaseResult<BeersCollection, Error>
    ) {
        val argumentCaptor = argumentCaptor<() -> BeersCollection>()

        whenever(connectionRequiredUseCaseRunnerStub.run(argumentCaptor.capture())).thenReturn(
            connectionRequiredUseCaseRunnerResult
        )

        val actual = sut.getBeersByName(TestConstants.FAKE_BEER_NAME)
        val expected = connectionRequiredUseCaseRunnerResult

        assertEquals(expected, actual)
    }
}
