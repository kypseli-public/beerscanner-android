package com.kypseli.beerscanner.beerlist.di

import com.kypseli.beerscanner.application.di.ActivityScope
import com.kypseli.beerscanner.beerlist.presentation.BeerListActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [BeerListModule::class])
interface IBeerListComponent {

    fun inject(activity: BeerListActivity)

    @Subcomponent.Builder
    interface IBuilder {

        fun beerListModule(module: BeerListModule): IBuilder
        fun build(): IBeerListComponent
    }
}
