package com.kypseli.beerscanner.beerlist.data

import com.kypseli.beerscanner.beerlist.domain.IBeersRepository
import com.kypseli.beerscanner.common.data.IModelMapper

class BeersRepository(
    private val beersApi: IBeerListApi,
    private val beerListModelMapper:
    IModelMapper<BeersCollection, com.kypseli.beerscanner.beerlist.domain.BeersCollection>
) : IBeersRepository {

    override fun getBeersByName(
        beerName: String
    ): com.kypseli.beerscanner.beerlist.domain.BeersCollection {

        val response = beersApi.getBeersByName(beerName).execute()
        val beerListResponseBody = response.body()!!
        val beersCollectionDataModel = BeersCollection(beerListResponseBody)

        return beerListModelMapper.fromDataModelToDomainModel(
            beersCollectionDataModel
        )
    }
}
