package com.kypseli.beerscanner.beerlist.domain

import com.kypseli.beerscanner.common.domain.IDomainModel

data class Beer(
    val id: Int,
    val name: String,
    val tagline: String,
    val description: String,
    val abv: Float,
    val imageUrl: String?
) : IDomainModel
