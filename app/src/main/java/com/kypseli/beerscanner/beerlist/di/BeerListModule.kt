package com.kypseli.beerscanner.beerlist.di

import androidx.appcompat.app.AppCompatActivity
import com.kypseli.beerscanner.application.di.ActivityScope
import com.kypseli.beerscanner.beerlist.data.BeersCollectionMapper
import com.kypseli.beerscanner.beerlist.data.BeersRepository
import com.kypseli.beerscanner.beerlist.data.IBeerListApi
import com.kypseli.beerscanner.beerlist.domain.BeersCollection
import com.kypseli.beerscanner.beerlist.domain.GetBeersByNameUseCase
import com.kypseli.beerscanner.beerlist.domain.IBeersRepository
import com.kypseli.beerscanner.beerlist.presentation.BeerListPresenter
import com.kypseli.beerscanner.common.data.ConnectivityChecker
import com.kypseli.beerscanner.common.data.IModelMapper
import com.kypseli.beerscanner.common.domain.ConnectionRequiredUseCaseRunner
import com.kypseli.beerscanner.common.domain.Error
import com.kypseli.beerscanner.common.domain.IConnectivityChecker
import com.kypseli.beerscanner.common.presentation.contracts.ICancellableExecutor
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class BeerListModule(private val activity: AppCompatActivity) {

    @Provides
    @ActivityScope
    internal fun provideBeerListApi(retrofit: Retrofit): IBeerListApi {
        return retrofit.create(IBeerListApi::class.java)
    }

    @Provides
    @ActivityScope
    internal fun provideBeersCollectionMapper()
            : IModelMapper<com.kypseli.beerscanner.beerlist.data.BeersCollection, BeersCollection> {
        return BeersCollectionMapper()
    }

    @Provides
    @ActivityScope
    internal fun provideBeersRepository(
        beerListApi: IBeerListApi,
        beersCollectionMapper
        : IModelMapper<com.kypseli.beerscanner.beerlist.data.BeersCollection, BeersCollection>
    ): IBeersRepository {
        return BeersRepository(beerListApi, beersCollectionMapper)
    }

    @Provides
    @ActivityScope
    internal fun provideConnectivityChecker(): IConnectivityChecker {
        return ConnectivityChecker(activity)
    }

    @Provides
    @ActivityScope
    internal fun provideConnectionRequiredUseCaseRunner(
        connectivityChecker: IConnectivityChecker
    ): ConnectionRequiredUseCaseRunner<BeersCollection, Error> {
        return ConnectionRequiredUseCaseRunner(connectivityChecker)
    }

    @Provides
    @ActivityScope
    internal fun provideGetBeersByNameUseCase(
        connectionRequiredUseCaseRunner: ConnectionRequiredUseCaseRunner<BeersCollection, Error>,
        beersRepository: IBeersRepository
    ): GetBeersByNameUseCase {
        return GetBeersByNameUseCase(connectionRequiredUseCaseRunner, beersRepository)
    }

    @Provides
    @ActivityScope
    internal fun provideBeerListPresenter(
        cancellableExecutor: ICancellableExecutor,
        getBeersByNameUseCase: GetBeersByNameUseCase
    ): BeerListPresenter {
        return BeerListPresenter(cancellableExecutor, getBeersByNameUseCase)
    }
}
