package com.kypseli.beerscanner.beerlist.data

import com.kypseli.beerscanner.common.data.ApiConstants
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface IBeerListApi {

    @GET(ApiConstants.API_BEERS_PATH)
    fun getBeersByName(@Query(ApiConstants.API_BEER_NAME_QUERY_PARAM) beerName: String): Call<List<Beer>>
}
