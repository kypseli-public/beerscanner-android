package com.kypseli.beerscanner.beerlist.domain

import com.kypseli.beerscanner.common.domain.*

class GetBeersByNameUseCase(
    private val connectionRequiredUseCaseRunner
    : ConnectionRequiredUseCaseRunner<BeersCollection, Error>,
    private val beersRepository: IBeersRepository
) {

    fun getBeersByName(searchName: String): UseCaseResult<BeersCollection, Error> {
        return connectionRequiredUseCaseRunner.run {
            beersRepository.getBeersByName(searchName)
        }
    }
}
