package com.kypseli.beerscanner.beerlist.presentation.contracts

import com.kypseli.beerscanner.beerlist.domain.Beer
import com.kypseli.beerscanner.beerlist.domain.BeersCollection
import com.kypseli.beerscanner.common.presentation.contracts.IPresenterView

interface IBeerListContract {

    interface IBeerListView : IPresenterView {
        fun showLoading()
        fun hideLoading()
        fun showConnectionRequiredError()
        fun showGeneralErrorMessage()
        fun showBeerListItems(beersCollection: BeersCollection)
        fun navigateToBeerDetailScreen(beer: Beer)
    }

    interface IBeerListPresenter {

        fun onBeerSearchTextChanged(searchName: String)
        fun onBeerListItemClicked(beer: Beer)
    }
}
