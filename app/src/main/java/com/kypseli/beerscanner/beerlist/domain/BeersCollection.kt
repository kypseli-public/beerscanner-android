package com.kypseli.beerscanner.beerlist.domain

import com.kypseli.beerscanner.common.domain.IDomainModel

data class BeersCollection(val beers: List<Beer>) : IDomainModel
