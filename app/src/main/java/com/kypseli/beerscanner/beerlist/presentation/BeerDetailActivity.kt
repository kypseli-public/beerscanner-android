package com.kypseli.beerscanner.beerlist.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.kypseli.beerscanner.R
import com.kypseli.beerscanner.beerlist.domain.Beer
import com.kypseli.beerscanner.common.presentation.BaseActivity
import com.kypseli.beerscanner.common.presentation.loadImageFromUrl
import kotlinx.android.synthetic.main.activity_beer_detail.*

class BeerDetailActivity : BaseActivity() {

    companion object {

        private const val BEER_TITLE_INTENT_EXTRA_NAME = "BEER_TITLE"
        private const val BEER_TAGLINE_INTENT_EXTRA_NAME = "BEER_TAGLINE"
        private const val BEER_DESCRIPTION_INTENT_EXTRA_NAME = "BEER_DESCRIPTION"
        private const val BEER_ABV_INTENT_EXTRA_NAME = "BEER_ABV"
        private const val BEER_IMAGEURL_INTENT_EXTRA_NAME = "BEER_IMAGE_URL"

        fun buildCallingIntent(context: Context, beer: Beer): Intent {
            val intent = Intent(context, BeerDetailActivity::class.java)
            intent.putExtra(BEER_TITLE_INTENT_EXTRA_NAME, beer.name)
            intent.putExtra(BEER_TAGLINE_INTENT_EXTRA_NAME, beer.tagline)
            intent.putExtra(BEER_DESCRIPTION_INTENT_EXTRA_NAME, beer.description)
            intent.putExtra(BEER_ABV_INTENT_EXTRA_NAME, beer.abv)
            intent.putExtra(BEER_IMAGEURL_INTENT_EXTRA_NAME, beer.imageUrl)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViews()
    }

    override fun getLayoutResId() = R.layout.activity_beer_detail

    private fun setupViews() {
        beerNameTextView.text = intent.getStringExtra(BEER_TITLE_INTENT_EXTRA_NAME)
        beerTaglineTextView.text = intent.getStringExtra(BEER_TAGLINE_INTENT_EXTRA_NAME)
        beerDescriptionTextView.text = intent.getStringExtra(BEER_DESCRIPTION_INTENT_EXTRA_NAME)
        beerAbvTextView.text = intent.getFloatExtra(BEER_ABV_INTENT_EXTRA_NAME, 0F).toString()

        loadImageFromUrl(beerImageView, intent.getStringExtra(BEER_IMAGEURL_INTENT_EXTRA_NAME))
    }
}
