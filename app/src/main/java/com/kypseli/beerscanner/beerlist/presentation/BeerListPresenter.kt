package com.kypseli.beerscanner.beerlist.presentation

import com.kypseli.beerscanner.beerlist.domain.Beer
import com.kypseli.beerscanner.beerlist.domain.GetBeersByNameUseCase
import com.kypseli.beerscanner.beerlist.presentation.contracts.IBeerListContract
import com.kypseli.beerscanner.common.domain.Error
import com.kypseli.beerscanner.common.domain.fold
import com.kypseli.beerscanner.common.presentation.AsyncPresenter
import com.kypseli.beerscanner.common.presentation.contracts.ICancellableExecutor

class BeerListPresenter(
    executor: ICancellableExecutor,
    private val getBeersByNameUseCase: GetBeersByNameUseCase
) : AsyncPresenter<IBeerListContract.IBeerListView>(executor),
    IBeerListContract.IBeerListPresenter {

    override fun onBeerSearchTextChanged(searchName: String) {
        if (searchName.isNotEmpty()) {
            getBeersByName(searchName)
        }
    }

    override fun onBeerListItemClicked(beer: Beer) {
        view?.navigateToBeerDetailScreen(beer)
    }

    private fun getBeersByName(searchName: String) {
        view?.showLoading()
        executeAsync {
            val result = getBeersByNameUseCase.getBeersByName(searchName)
            executeUi {
                result.fold(
                    {
                        view?.showBeerListItems(it)
                    },
                    {
                        when (it) {
                            is Error.ConnectionRequired -> view?.showConnectionRequiredError()
                            else -> view?.showGeneralErrorMessage()
                        }
                    }
                )
                view?.hideLoading()
            }
        }
    }
}
