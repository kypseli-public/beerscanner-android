package com.kypseli.beerscanner.beerlist.domain

interface IBeersRepository {

    fun getBeersByName(beerName: String) : BeersCollection
}
