package com.kypseli.beerscanner.beerlist.data

import com.kypseli.beerscanner.beerlist.domain.Beer
import com.kypseli.beerscanner.common.data.IModelMapper

class BeersCollectionMapper
    : IModelMapper<BeersCollection, com.kypseli.beerscanner.beerlist.domain.BeersCollection> {

    override fun fromDataModelToDomainModel(
        dataModel: BeersCollection
    ): com.kypseli.beerscanner.beerlist.domain.BeersCollection {

        val beerList = arrayListOf<Beer>()

        dataModel.beers.forEach {
            beerList.add(
                Beer(
                    it.id,
                    it.name,
                    it.tagline,
                    it.description,
                    it.abv,
                    it.imageUrl
                )
            )
        }

        return com.kypseli.beerscanner.beerlist.domain.BeersCollection(beerList)
    }
}
