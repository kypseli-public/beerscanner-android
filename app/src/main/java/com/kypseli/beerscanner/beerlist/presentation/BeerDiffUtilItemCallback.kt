package com.kypseli.beerscanner.beerlist.presentation

import androidx.recyclerview.widget.DiffUtil
import com.kypseli.beerscanner.beerlist.domain.Beer

class BeerDiffUtilItemCallback : DiffUtil.ItemCallback<Beer>() {

    override fun areItemsTheSame(oldItem: Beer, newItem: Beer): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Beer, newItem: Beer): Boolean = oldItem == newItem
}
