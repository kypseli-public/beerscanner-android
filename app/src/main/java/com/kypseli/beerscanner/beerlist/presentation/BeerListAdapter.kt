package com.kypseli.beerscanner.beerlist.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.kypseli.beerscanner.R
import com.kypseli.beerscanner.beerlist.domain.Beer
import com.kypseli.beerscanner.common.presentation.loadImageFromUrl

class BeerListAdapter(private val listener: Listener) :
    ListAdapter<Beer, BeerListAdapter.BeerListItemViewHolder>(BeerDiffUtilItemCallback()) {

    interface Listener {
        fun onBeerListItemClicked(beer: Beer)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerListItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_beer, parent, false)
        return BeerListItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: BeerListItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class BeerListItemViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {
        private val rootView: View =
            itemView.findViewById(R.id.rootView)
        private val beerImageView: ImageView =
            itemView.findViewById(R.id.beerImageView)
        private val beerNameTextView: TextView =
            itemView.findViewById(R.id.beerNameTextView)
        private val beerTaglineTextView: TextView =
            itemView.findViewById(R.id.beerTaglineTextView)

        private lateinit var beer: Beer

        fun bind(beer: Beer) {
            this.beer = beer

            loadImageFromUrl(beerImageView, beer.imageUrl)

            beerNameTextView.text = beer.name
            beerTaglineTextView.text = beer.tagline

            attachListeners(beer)
        }

        private fun attachListeners(beer: Beer) {
            rootView.setOnClickListener {
                listener.onBeerListItemClicked(beer)
            }
        }
    }
}
