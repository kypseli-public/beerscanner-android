package com.kypseli.beerscanner.beerlist.data

import com.kypseli.beerscanner.common.data.IDataModel

data class BeersCollection(val beers: List<Beer>) : IDataModel
