package com.kypseli.beerscanner.beerlist.presentation

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.kypseli.beerscanner.R
import com.kypseli.beerscanner.application.di.ActivityScope
import com.kypseli.beerscanner.application.di.IAppComponent
import com.kypseli.beerscanner.beerlist.di.BeerListModule
import com.kypseli.beerscanner.beerlist.domain.Beer
import com.kypseli.beerscanner.beerlist.domain.BeersCollection
import com.kypseli.beerscanner.beerlist.presentation.contracts.IBeerListContract
import com.kypseli.beerscanner.common.presentation.BaseActivityWithPresenter
import kotlinx.android.synthetic.main.activity_beer_list.*

@ActivityScope
class BeerListActivity
    : BaseActivityWithPresenter<IBeerListContract.IBeerListView, BeerListPresenter>(),
    IBeerListContract.IBeerListView, BeerListAdapter.Listener {

    private lateinit var adapter: BeerListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        attachListeners()
        setupAdapter()
    }

    override fun injectDependencies(appComponent: IAppComponent) {
        appComponent.beerListComponent()
            .beerListModule(BeerListModule(this))
            .build()
            .inject(this)
    }

    override fun getLayoutResId(): Int = R.layout.activity_beer_list

    override fun getPresenterView(): IBeerListContract.IBeerListView = this

    override fun showLoading() {
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar?.visibility = View.GONE
    }

    override fun showConnectionRequiredError() {
        showSnackbar(rootView, R.string.connection_required_message)
    }

    override fun showGeneralErrorMessage() {
        showSnackbar(rootView, R.string.general_error_message)
    }

    override fun showBeerListItems(beersCollection: BeersCollection) {
        adapter.submitList(beersCollection.beers)
    }

    override fun navigateToBeerDetailScreen(beer: Beer) {
        startActivity(BeerDetailActivity.buildCallingIntent(this, beer))
    }

    override fun onBeerListItemClicked(beer: Beer) {
        presenter.onBeerListItemClicked(beer)
    }

    private fun attachListeners() {
        beerSearchEditText.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                presenter.onBeerSearchTextChanged(s.toString())
            }
        })
    }

    private fun setupAdapter() {
        adapter = BeerListAdapter(this)
        beerListRecyclerView.adapter = adapter
    }
}
