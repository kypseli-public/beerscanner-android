package com.kypseli.beerscanner.common.presentation

import android.os.Bundle
import com.kypseli.beerscanner.application.App
import com.kypseli.beerscanner.application.di.ActivityScope
import com.kypseli.beerscanner.application.di.IAppComponent
import com.kypseli.beerscanner.common.presentation.contracts.IPresenterView
import javax.inject.Inject

@ActivityScope
abstract class BaseActivityWithPresenter<V : IPresenterView, P : AsyncPresenter<V>>
    : BaseActivity() {

    @Inject
    lateinit var presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies((this.application as App).component)
        presenter.onViewCreated(getPresenterView())
    }

    override fun onResume() {
        super.onResume()
        presenter.onViewVisible()
    }

    override fun onDestroy() {
        presenter.onViewDestroyed()
        super.onDestroy()
    }

    abstract fun injectDependencies(appComponent: IAppComponent)

    abstract fun getPresenterView(): V
}
