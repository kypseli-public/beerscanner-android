package com.kypseli.beerscanner.common.domain.exceptions

class ConnectionRequiredException : Exception()
