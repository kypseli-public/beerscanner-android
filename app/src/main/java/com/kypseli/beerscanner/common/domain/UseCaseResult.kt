package com.kypseli.beerscanner.common.domain

sealed class UseCaseResult<out S : IDomainModel, out E : Error> {

    data class Success<out S : IDomainModel>(val value: S) : UseCaseResult<S, Nothing>()
    data class Failure<out E : Error>(val value: E) : UseCaseResult<Nothing, E>()

    val isSuccess get() = this is Success
    val isFailure get() = this is Failure

    fun <S : IDomainModel> success(a: S) = Success(a)
    fun <E : Error> failure(b: E) = Failure(b)
}

fun <S : IDomainModel, E : Error, T> UseCaseResult<S, E>.fold(
    success: (result: S) -> T,
    failure: (error: E) -> T
): T =
    when (this) {
        is UseCaseResult.Success -> success(value)
        is UseCaseResult.Failure -> failure(value)
    }
