package com.kypseli.beerscanner.common.data

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.kypseli.beerscanner.common.domain.IConnectivityChecker
import com.kypseli.beerscanner.common.domain.exceptions.ConnectionRequiredException

class ConnectivityChecker(private val context: Context) : IConnectivityChecker {

    @Throws(ConnectionRequiredException::class)
    override fun assertIsConnected() {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnected == true
        if (!isConnected) {
            throw ConnectionRequiredException()
        }
    }
}
