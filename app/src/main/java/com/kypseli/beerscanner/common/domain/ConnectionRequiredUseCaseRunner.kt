package com.kypseli.beerscanner.common.domain

open class ConnectionRequiredUseCaseRunner<R : IDomainModel, E : Error>(
    private val connectivityChecker: IConnectivityChecker
) : UseCaseRunner<R, E>() {

    override fun run(
        executeOperation: () -> R
    ): UseCaseResult<R, E> =
        super.run {
            connectivityChecker.assertIsConnected()
            executeOperation()
        }
}
