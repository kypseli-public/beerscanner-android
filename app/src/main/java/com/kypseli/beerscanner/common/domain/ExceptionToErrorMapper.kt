package com.kypseli.beerscanner.common.domain

import com.kypseli.beerscanner.common.domain.exceptions.ConnectionRequiredException

class ExceptionToErrorMapper {

    fun map(e: Exception): Error {
        return when (e) {
            is ConnectionRequiredException -> Error.ConnectionRequired
            else -> Error.UnknownError(e.message ?: "unknown")
        }
    }
}
