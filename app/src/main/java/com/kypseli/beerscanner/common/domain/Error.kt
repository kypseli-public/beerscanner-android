package com.kypseli.beerscanner.common.domain

sealed class Error {

    object ConnectionRequired : Error()
    data class UnknownError(val message: String) : Error()
}
