package com.kypseli.beerscanner.common.domain

import java.lang.Exception

open class UseCaseRunner<R : IDomainModel, E : Error> {

    private val exceptionToErrorMapper = ExceptionToErrorMapper()

    open fun run(
        executeOperation: () -> R
    ): UseCaseResult<R, E> {
        try {
            return UseCaseResult.Success(executeOperation())
        } catch (e: Exception) {
            return UseCaseResult.Failure(mapError(e))
        }
    }

    private fun mapError(e: Exception): E = exceptionToErrorMapper.map(e) as E
}
