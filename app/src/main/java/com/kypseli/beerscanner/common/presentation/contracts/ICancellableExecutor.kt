package com.kypseli.beerscanner.common.presentation.contracts

interface ICancellableExecutor {

    fun uiExecute(uiFun: suspend () -> Unit)
    fun asyncExecute(asyncFun: suspend () -> Unit)
    fun cancelAll()
}
