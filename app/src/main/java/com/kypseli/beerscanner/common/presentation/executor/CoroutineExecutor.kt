package com.kypseli.beerscanner.common.presentation.executor

import android.util.Log
import com.kypseli.beerscanner.common.presentation.contracts.ICancellableExecutor
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class CoroutineExecutor(private val asyncContext: CoroutineContext = Dispatchers.IO) :
    ICancellableExecutor, CoroutineScope {

    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job + loggingExceptionHandler

    private val loggingExceptionHandler = CoroutineExceptionHandler { _, t ->
        Log.e(javaClass.simpleName, t.message)
    }

    override fun uiExecute(uiFun: suspend () -> Unit) {
        launch {
            uiFun()
        }
    }

    override fun asyncExecute(asyncFun: suspend () -> Unit) {
        launch(asyncContext) {
            asyncFun()
        }
    }

    override fun cancelAll() {
        job.cancel()
    }
}
