package com.kypseli.beerscanner.common.data

class ApiConstants {

    companion object {
        const val API_BASE_PATH = "v2"
        const val API_BEERS_PATH = "beers"
        const val API_BEER_NAME_QUERY_PARAM = "beer_name"
    }
}
