package com.kypseli.beerscanner.common.presentation

import android.widget.ImageView
import com.kypseli.beerscanner.R
import com.squareup.picasso.Picasso

fun loadImageFromUrl(imageView: ImageView, imageUrl: String?) {
    Picasso
        .get()
        .load(imageUrl)
        .placeholder(R.drawable.ic_image_placeholder)
        .centerCrop()
        .fit().into(imageView)
}
