package com.kypseli.beerscanner.common.presentation

import com.kypseli.beerscanner.common.presentation.contracts.ICancellableExecutor
import com.kypseli.beerscanner.common.presentation.contracts.IPresenterView

abstract class AsyncPresenter<T : IPresenterView>(
    private val executor: ICancellableExecutor
) {

    protected var view: T? = null

    open fun onViewCreated(view: T) {
        this.view = view
    }

    open fun onViewDestroyed() {
        view = null
        cancelAllJobs()
    }

    open fun onViewVisible() {}

    protected fun executeAsync(asyncFun: suspend () -> Unit) {
        executor.asyncExecute { asyncFun() }
    }

    protected fun executeUi(uiFun: suspend () -> Unit) {
        executor.uiExecute { uiFun() }
    }

    private fun cancelAllJobs() {
        executor.cancelAll()
    }
}
