package com.kypseli.beerscanner.common.domain

import com.kypseli.beerscanner.common.domain.exceptions.ConnectionRequiredException

interface IConnectivityChecker {

    @Throws(ConnectionRequiredException::class)
    fun assertIsConnected()
}
