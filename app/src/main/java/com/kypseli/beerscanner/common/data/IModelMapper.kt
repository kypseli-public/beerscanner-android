package com.kypseli.beerscanner.common.data

import com.kypseli.beerscanner.common.domain.IDomainModel

interface IModelMapper<T : IDataModel, S : IDomainModel> {

    fun fromDataModelToDomainModel(dataModel : T) : S
}
