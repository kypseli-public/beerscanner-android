package com.kypseli.beerscanner.common.presentation

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.kypseli.beerscanner.application.di.ActivityScope

@ActivityScope
abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResId())
    }

    abstract fun getLayoutResId(): Int

    protected fun showSnackbar(rootView: View, messageResId: Int) {
        Snackbar.make(rootView, messageResId, Snackbar.LENGTH_LONG).show()
    }
}
