package com.kypseli.beerscanner.application.di

import com.kypseli.beerscanner.application.App
import com.kypseli.beerscanner.beerlist.di.IBeerListComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AppModule::class, NetworkModule::class, NetworkUrlsModule::class]
)
interface IAppComponent {

    fun inject(app: App)

    fun beerListComponent(): IBeerListComponent.IBuilder
}
