package com.kypseli.beerscanner.application

import android.app.Application
import com.kypseli.beerscanner.application.di.AppModule
import com.kypseli.beerscanner.application.di.DaggerIAppComponent
import com.kypseli.beerscanner.application.di.IAppComponent

open class App : Application() {

    val component: IAppComponent by lazy {
        initializeComponent()
    }

    companion object {
        private lateinit var instance: App

        fun getInstance(): App {
            return instance
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        component.inject(this)
    }

    protected open fun initializeComponent(): IAppComponent {
        return DaggerIAppComponent
            .builder()
            .appModule(AppModule(this))
            .build()
    }
}
