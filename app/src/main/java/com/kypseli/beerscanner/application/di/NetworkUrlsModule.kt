package com.kypseli.beerscanner.application.di

import com.kypseli.beerscanner.common.data.ApiConstants
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class NetworkUrlsModule {

    companion object {
        const val PROVIDED_API_BASE_URL_NAME = "apiBaseName"
        private const val API_DOMAIN = "https://api.punkapi.com/"
    }

    @Provides
    @Named(PROVIDED_API_BASE_URL_NAME)
    fun provideApiUrl(): String = "$API_DOMAIN/${ApiConstants.API_BASE_PATH}/"
}
