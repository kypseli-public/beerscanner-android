package com.kypseli.beerscanner.application.di

import javax.inject.Scope
import kotlin.annotation.Retention

@Scope
@MustBeDocumented
@Retention(value = AnnotationRetention.RUNTIME)
annotation class ActivityScope
