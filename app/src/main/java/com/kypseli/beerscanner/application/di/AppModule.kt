package com.kypseli.beerscanner.application.di

import com.kypseli.beerscanner.application.App
import com.kypseli.beerscanner.common.presentation.contracts.ICancellableExecutor
import com.kypseli.beerscanner.common.presentation.executor.CoroutineExecutor
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: App) {

    @Provides
    @Singleton
    fun provideApp() = app

    @Provides
    fun provideExecutor(): ICancellableExecutor {
        return CoroutineExecutor()
    }
}
