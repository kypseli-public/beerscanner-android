package com.kypseli.beerscanner

import com.kypseli.beerscanner.application.App
import com.kypseli.beerscanner.application.di.AppModule
import com.kypseli.beerscanner.application.di.IAppComponent
import com.kypseli.beerscanner.di.DaggerITestAppComponent
import com.kypseli.beerscanner.di.TestNetworkUrlsModule

class TestApp : App() {

    override fun initializeComponent(): IAppComponent {
        return DaggerITestAppComponent
            .builder()
            .appModule(AppModule(this))
            .testNetworkUrlsModule(TestNetworkUrlsModule())
            .build()
    }
}
