package com.kypseli.beerscanner

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import com.kypseli.beerscanner.beerlist.presentation.BeerListActivity
import com.kypseli.beerscanner.common.data.ApiConstants
import com.kypseli.beerscanner.di.ITestAppComponent
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test

class BeerListFunctionalTest {

    companion object {
        private const val TEST_BEER_SEARCH_TEXT = "F"

        private const val FAKE_RESPONSE_BODY = "" +
                "[" +
                "    {" +
                "        \"id\": 1," +
                "        \"name\": \"Fake Beer 1\"," +
                "        \"tagline\": \"Fake tagline 1\"," +
                "        \"description\": \"Fake description 1\"," +
                "        \"image_url\": null," +
                "        \"abv\": 4.7" +
                "    }," +
                "    {" +
                "        \"id\": 2," +
                "        \"name\": \"Fake Beer 2\"," +
                "        \"tagline\": \"Fake tagline 2\"," +
                "        \"description\": \"Fake description 1\"," +
                "        \"image_url\": null," +
                "        \"abv\": 4.8" +
                "    }" +
                "]"
    }

    @get:Rule
    val activityRule = ActivityTestRule<BeerListActivity>(BeerListActivity::class.java)

    @Test
    fun beerSearchSucceeds() {
        setupMockServer(200)

        onView(withId(R.id.beerListRecyclerView))
            .check(
                RecyclerViewItemCountAssertion(
                    0,
                    "Guard: RecyclerView item count before filling search EditText should be 0"
                )
            )

        fillBeerSearchEditText()

        onView(withId(R.id.progressBar)).perform(WaitAction(isDisplayed()))
        onView(withId(R.id.progressBar)).perform(WaitAction(not(isDisplayed())))

        onView(withId(R.id.beerListRecyclerView))
            .check(
                RecyclerViewItemCountAssertion(
                    2,
                    "RecyclerView item count after filling search EditText should be 2"
                )
            )
    }

    @Test
    fun beerSearchServerError() {
        setupMockServer(500)
        fillBeerSearchEditText()
        assertGeneralErrorSnackbarShown()
    }

    private fun setupMockServer(httpCodeToReturn: Int) {
        getMockWebServer().setDispatcher(object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when (request.path) {
                    buildFullApiPathForEndpoint() -> {
                        MockResponse().setResponseCode(httpCodeToReturn)
                            .setBody(FAKE_RESPONSE_BODY)
                    }
                    else -> {
                        MockResponse().setResponseCode(404)
                    }
                }
            }
        })
    }

    private fun fillBeerSearchEditText() {
        onView(withId(R.id.beerSearchEditText)).perform(
            typeText(TEST_BEER_SEARCH_TEXT)
        )
    }

    private fun assertGeneralErrorSnackbarShown() {
        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(withText(R.string.general_error_message)))
    }

    private fun buildFullApiPathForEndpoint(): String =
        "/${ApiConstants.API_BASE_PATH}/${ApiConstants.API_BEERS_PATH}" +
                "?${ApiConstants.API_BEER_NAME_QUERY_PARAM}=$TEST_BEER_SEARCH_TEXT"

    private fun getMockWebServer(): MockWebServer = getTestAppComponent().getMockWebServer()

    private fun getTestAppComponent(): ITestAppComponent =
        getTestApplication().component as ITestAppComponent

    private fun getTestApplication(): TestApp = activityRule.activity.application as TestApp
}
