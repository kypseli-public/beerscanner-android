package com.kypseli.beerscanner

import android.view.View
import androidx.test.espresso.PerformException
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.util.HumanReadables
import org.hamcrest.Matcher
import org.hamcrest.Matchers.any
import java.util.concurrent.TimeoutException

class WaitAction(
    private val matcher: Matcher<View>
) : ViewAction {

    companion object {
        private const val TIME_OUT_MS = 1000
    }

    override fun getDescription(): String {
        return "Waiting for specific action to occur"
    }

    override fun getConstraints(): Matcher<View> {
        return any(View::class.java)
    }

    override fun perform(uiController: UiController, view: View?) {
        val startTime = System.currentTimeMillis()
        val endTime = startTime + TIME_OUT_MS

        while (System.currentTimeMillis() < endTime) {
            if (matcher.matches(view)) {
                return
            }
            uiController.loopMainThreadUntilIdle()
        }

        throw PerformException.Builder()
            .withActionDescription(this.description)
            .withViewDescription(HumanReadables.describe(view))
            .withCause(TimeoutException())
            .build()
    }
}
