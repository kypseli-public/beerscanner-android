package com.kypseli.beerscanner.di

import com.kypseli.beerscanner.application.di.NetworkUrlsModule.Companion.PROVIDED_API_BASE_URL_NAME
import com.kypseli.beerscanner.common.data.ApiConstants
import dagger.Module
import dagger.Provides
import okhttp3.mockwebserver.MockWebServer
import javax.inject.Named
import javax.inject.Singleton

@Module
class TestNetworkUrlsModule {

    @Singleton
    @Provides
    fun provideMockWebServer(): MockWebServer {
        var mockWebServer: MockWebServer? = null
        val thread = Thread(Runnable {
            mockWebServer = MockWebServer()
            mockWebServer?.start()
        })
        thread.start()
        thread.join()
        return mockWebServer ?: throw NullPointerException()
    }

    @Provides
    @Named(PROVIDED_API_BASE_URL_NAME)
    fun provideApiUrl(mockWebServer: MockWebServer): String {
        var mockServerUrl = ""
        val thread = Thread(Runnable {
            mockServerUrl = mockWebServer.url("/" + ApiConstants.API_BASE_PATH + "/").toString()
        })
        thread.start()
        thread.join()
        return mockServerUrl
    }
}
