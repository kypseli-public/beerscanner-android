package com.kypseli.beerscanner.di

import com.kypseli.beerscanner.application.di.AppModule
import com.kypseli.beerscanner.application.di.NetworkModule
import com.kypseli.beerscanner.application.di.*
import dagger.Component
import okhttp3.mockwebserver.MockWebServer
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AppModule::class, NetworkModule::class, TestNetworkUrlsModule::class]
)
interface ITestAppComponent : IAppComponent {

    fun getMockWebServer(): MockWebServer
}
