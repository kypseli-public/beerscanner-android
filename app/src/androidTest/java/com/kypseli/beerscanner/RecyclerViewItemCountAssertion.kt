package com.kypseli.beerscanner

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import org.hamcrest.CoreMatchers

class RecyclerViewItemCountAssertion(
    private val count: Int,
    private val assertionMessage: String?
) : ViewAssertion {

    override fun check(view: View, noViewFoundException: NoMatchingViewException?) {
        if (noViewFoundException != null) {
            throw noViewFoundException
        }

        check(view is RecyclerView) { "The asserted view is not a RecyclerView" }
        checkNotNull(view.adapter) { "No adapter assigned to RecyclerView" }

        assertThat(assertionMessage, view.adapter!!.itemCount, CoreMatchers.equalTo(count))
    }
}
