# Changelog

All notable changes from beerscanner-android project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2020-05-18

- **Added**: Beer detail screen
- **Added**: Beer list screen with elements retrieved from Punk API using Retrofit2
- **Added**: Use case logics with connectivity check, result and error treatment
- **Added**: Base activity and async presenter with coroutines
- **Added**: Dependency injection setup
- **Added**: Android project structure setup
